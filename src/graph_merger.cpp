#include "graph_merger.h"

namespace srrg_mapper2d {

  GraphMerger::GraphMerger(SimpleGraph* sgraph){
    _sgraph = sgraph;
    _vertex_merging_distance = 1;

    //create projector of 2*M_PI 
    float fov = 2*M_PI; 
    int num_ranges = 1440; //0.25 degrees resolution
    _projector = new Projector2D;
    _projector->setFov(fov);
    _projector->setNumRanges(num_ranges);

    //Init Cloud Merger
    _c2DMerger = new Cloud2DWithTrajectoryMerger;
    
    //Init Cloud Aligner
    _c2DAligner=new Cloud2DAligner(_projector);
    _c2DAligner->useNNCorrespondenceFinder();

    _inliers_distance = 0.05;
    _min_inliers_ratio = 0.8;
    _min_num_correspondences = 100;

    _c2DAligner->setInliersDistance(_inliers_distance);
    _c2DAligner->setMinInliersRatio(_min_inliers_ratio);
    _c2DAligner->setMinNumCorrespondences(_min_num_correspondences);
  }

  void GraphMerger::removeMergedVertices(OptimizableGraph::VertexSet& vertices_to_remove) {

    std::cerr << "Removing vertices: "; 
    for (OptimizableGraph::VertexSet::iterator it = vertices_to_remove.begin(); it != vertices_to_remove.end(); it++){
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *it;
      std::cerr << v->id() << " ";
    }
    std::cerr << std::endl;

    for (OptimizableGraph::VertexSet::iterator it = vertices_to_remove.begin(); it != vertices_to_remove.end(); it++){
      VertexSE2* v = (VertexSE2*) *it;
      _sgraph->removeVertex(v);
    }
  }
  
  void GraphMerger::computeNewNeighbors(OptimizableGraph::VertexSet& vertices_to_remove, OptimizableGraph::VertexSet& vertices_to_connect) {
    vertices_to_connect.clear();

    for (OptimizableGraph::VertexSet::iterator it = vertices_to_remove.begin(); it != vertices_to_remove.end(); it++){
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *it;
      
      for (OptimizableGraph::EdgeSet::iterator ite=v->edges().begin(); ite!=v->edges().end(); ++ite){
	OptimizableGraph::Edge* ev = (OptimizableGraph::Edge*) *ite;

	OptimizableGraph::Vertex* fromv = (OptimizableGraph::Vertex*) ev->vertices()[0];
	OptimizableGraph::Vertex* tov = (OptimizableGraph::Vertex*) ev->vertices()[1];
	std::cerr << "Edge from: " << fromv->id() << " to: " << tov->id() << std::endl;

	if (vertices_to_remove.find(fromv)==vertices_to_remove.end()){
	  vertices_to_connect.insert(fromv);
	}//else: fromv has also been merged
	if (vertices_to_remove.find(tov)==vertices_to_remove.end()){
	  vertices_to_connect.insert(tov);
	}//else: tov has also been merged
      }
    }

    std::cerr << "Vertices to connect: ";
    for (OptimizableGraph::VertexSet::iterator it = vertices_to_connect.begin(); it != vertices_to_connect.end(); it++){
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *it;
      std::cerr << v->id() << " ";
    }
    std::cerr << std::endl;
  }
  /*
  void GraphMerger::merge(VertexSE2* reference_vertex, VertexSE2* other_vertex, Eigen::Isometry2f& transform) {
    //Merges cloud from other_vertex into reference_vertex. Returns transform used in the alignment.
    
    //Using directly the relative pose estimation for merging
    transform = reference_vertex->estimate().toIsometry().cast<float>().inverse() * other_vertex->estimate().toIsometry().cast<float>(); 

    Cloud2DWithTrajectory* cloud_merged = new Cloud2DWithTrajectory;
    Cloud2DWithTrajectory* cloud_origin = dynamic_cast<Cloud2DWithTrajectory*>(_sgraph->vertexCloud(reference_vertex));
    Cloud2DWithTrajectory* cloud_current = dynamic_cast<Cloud2DWithTrajectory*>(_sgraph->vertexCloud(other_vertex));
    
    //Clouds merging
    std::cerr << "Merging with initial transf: " << t2v(transform).transpose() << std::endl;
    _c2DMerger->setReference(cloud_origin);	  
    _c2DMerger->setCurrent(cloud_current);
    _c2DMerger->compute(transform);
    cloud_merged = _c2DMerger->cloudMerged();
    transform = _c2DMerger->transform();
    std::cerr << "Transf after merging: " << t2v(transform).transpose() << std::endl;
   
    //logging alignment
    char align_logfilename[1024];
    sprintf(align_logfilename, "align-%05d-%05d-%05d.dat", _sgraph->currentVertex()->id(), reference_vertex->id(), other_vertex->id());
    ofstream log_stream(align_logfilename);
    log_stream << _sgraph->currentVertex()->id() << " " << reference_vertex->id() << " " << other_vertex->id() << " " << t2v(transform).transpose() << endl;
    //logging merge
    char filename1[100],filename2[100];
    sprintf(filename1, "cloud_merge_%d_%d.dat", _sgraph->currentVertex()->id(), reference_vertex->id());
    sprintf(filename2, "cloud_merge_%d_%d.dat", _sgraph->currentVertex()->id(), other_vertex->id());
    ofstream os1(filename1), os2(filename2);
    cloud_origin->save(os1);
    cloud_current->save(os2);
    char filenamem[100];
    sprintf(filenamem, "cloud_merge_%d_%d_%d.dat", _sgraph->currentVertex()->id(), reference_vertex->id(), other_vertex->id());
    ofstream osm(filenamem);
    cloud_merged->save(osm);

    //Replacing old reference cloud by merged one
    Cloud2DWithTrajectory* c = (Cloud2DWithTrajectory*) _sgraph->vertexCloud(reference_vertex);
    delete c;
    Eigen::Vector3f colorM(0.6, 0, 0);
    cloud_merged->setColor(colorM);
    _sgraph->verticesClouds()[reference_vertex] = cloud_merged;    
  }*/

  
  void GraphMerger::merge(VertexSE2* reference_vertex, VertexSE2* other_vertex) {
    //Merges cloud from other_vertex into reference_vertex.

    Cloud2DWithTrajectory* cloud_origin = dynamic_cast<Cloud2DWithTrajectory*>(_sgraph->vertexCloud(reference_vertex));
    Cloud2DWithTrajectory* cloud_current = dynamic_cast<Cloud2DWithTrajectory*>(_sgraph->vertexCloud(other_vertex));
    
    //Clouds merging
    _c2DMerger->setReference(cloud_origin);	  
    _c2DMerger->setCurrent(cloud_current);
    _c2DMerger->compute();
   Cloud2DWithTrajectory* cloud_merged = _c2DMerger->cloudMerged();

   /*
    //logging merge
    char filename1[100],filename2[100];
    sprintf(filename1, "cloud_merge_%d_%d.dat", _sgraph->currentVertex()->id(), reference_vertex->id());
    sprintf(filename2, "cloud_merge_%d_%d.dat", _sgraph->currentVertex()->id(), other_vertex->id());
    ofstream os1(filename1), os2(filename2);
    cloud_origin->save(os1);
    cloud_current->save(os2);
    char filenamem[100];
    sprintf(filenamem, "cloud_merge_%d_%d_%d.dat", _sgraph->currentVertex()->id(), reference_vertex->id(), other_vertex->id());
    ofstream osm(filenamem);
    cloud_merged->save(osm);
   */
   
    //Replacing old reference cloud by merged one
    Cloud2DWithTrajectory* c = (Cloud2DWithTrajectory*) _sgraph->vertexCloud(other_vertex);
    delete c;
    Eigen::Vector3f colorM(0.6, 0, 0);
    cloud_merged->setColor(colorM);
    _sgraph->verticesClouds()[other_vertex] = cloud_merged;    

  }


  /*
  void GraphMerger::merge(OptimizableGraph::VertexSet& vertices){
    if (vertices.size() < 2){
      std::cerr << "Need at least 2 vertices to merge" << std::endl;
      return;
    }
    OrderedVertexSet vertices_ordered;
    for (OptimizableGraph::VertexSet::iterator itv = vertices.begin(); itv!= vertices.end(); itv++){
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *itv;
      vertices_ordered.insert(v);
    }
    std::cerr << "Merging vertices: ";
    for (OrderedVertexSet::iterator itv = vertices_ordered.begin(); itv!= vertices_ordered.end(); itv++){
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *itv;
      std::cerr << v->id() << " ";
    }
    std::cerr << std::endl;
    
    //Vertex with lowest id is the reference in which we will merge the newest clouds (new overwrites old).
    //Vertex with the highest id will keep the merged cloud after the process.
    VertexSE2* reference_vertex = 0;
    VertexSE2* current_vertex = 0;
    for (OptimizableGraph::VertexSet::iterator itv = vertices.begin(); itv!= vertices.end(); itv++){
      VertexSE2* v = (VertexSE2*) *itv;
      if (!reference_vertex || reference_vertex->id() > v->id())
	reference_vertex = v;
      if (!current_vertex || current_vertex->id() < v->id())
	current_vertex = v;
    }
    std::cerr << "Reference Vertex " << reference_vertex->id() << std::endl;
    std::cerr << "Current Vertex " << current_vertex->id() << std::endl;
  
    //We store which vertices have been finally merged and which transform has been used in the merge.
    std::map<OptimizableGraph::Vertex*, Eigen::Isometry2f> vertices_transforms_merged; 
    for (OptimizableGraph::VertexSet::iterator itv = vertices.begin(); itv!= vertices.end(); itv++){
      VertexSE2* v = (VertexSE2*) *itv;
      if (v != reference_vertex){
	Eigen::Isometry2f relative_transf;
	merge(reference_vertex, v, relative_transf);
	vertices_transforms_merged.insert(std::make_pair(v,relative_transf));
      }
    }

    
    vertices_ordered.erase(current_vertex);
    OptimizableGraph::VertexSet vertices_to_remove, vertices_to_connect;
    for (OrderedVertexSet::iterator itv = vertices_ordered.begin(); itv!= vertices_ordered.end(); itv++){
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *itv;
      vertices_to_remove.insert(v);
    }
    computeNewNeighbors(vertices_to_remove, vertices_to_connect);

    if (vertices_transforms_merged.size()){
      //we transform the cloud merged in the reference_vertex to the currentv
      Cloud2DWithTrajectory* c = (Cloud2DWithTrajectory*) _sgraph->vertexCloud(reference_vertex);

      Cloud2DWithTrajectory* cnew = new Cloud2DWithTrajectory;
      *cnew = *c;

      Eigen::Isometry2f movetransf = vertices_transforms_merged[current_vertex];
      //Eigen::Isometry2f movetransf = reference_vertex->estimate().toIsometry().inverse() *current_vertex->estimate().toIsometry();
      cnew->transformInPlace(movetransf.inverse());
      cnew->changeTrajectoryOrigin(current_vertex->estimate().toIsometry().cast<float>());	      
      cnew->setPose(current_vertex->estimate().toIsometry().cast<float>());
      _sgraph->verticesClouds()[current_vertex] = cnew;

      //logging merge
      char filename[100];
      sprintf(filename, "cloud_merge_after_%d_%d_%d.dat", _sgraph->currentVertex()->id(), reference_vertex->id(), current_vertex->id());
      ofstream os(filename);
      cnew->save(os);

      vertices_transforms_merged.insert(std::make_pair(reference_vertex, Eigen::Isometry2f::Identity())); //we insert ref_vertex because will be removed
      OptimizableGraph::VertexSet vertices_for_new_edges;
      for (std::map<OptimizableGraph::Vertex*, Eigen::Isometry2f>::iterator itv = vertices_transforms_merged.begin(); itv!= vertices_transforms_merged.end(); itv++){
	OptimizableGraph::Vertex* mergedv = itv->first;
	std::cerr << "Vertex_merged:" << mergedv->id() << std::endl;
	//check edges which are entering or leaving v;
	OptimizableGraph::EdgeSet tmp(mergedv->edges());
	for (OptimizableGraph::EdgeSet::iterator ite=tmp.begin(); ite!=tmp.end(); ++ite){
	  OptimizableGraph::Edge* ev = (OptimizableGraph::Edge*)*ite;
	  OptimizableGraph::Vertex* fromv = (OptimizableGraph::Vertex*) ev->vertices()[0];
	  OptimizableGraph::Vertex* tov = (OptimizableGraph::Vertex*) ev->vertices()[1];

	  std::cerr << "Edge from: " << fromv->id() << " to: " << tov->id();
	  if (vertices_transforms_merged.find(fromv)!=vertices_transforms_merged.end() &&
	      vertices_transforms_merged.find(tov)!=vertices_transforms_merged.end()){
	    //both vertices have been merged -> remove edge
	    std::cerr << "... Removing";
	    _sgraph->graph()->removeEdge(ev);
	  }else if (vertices_transforms_merged.find(fromv)!=vertices_transforms_merged.end() && 
		    fromv!=current_vertex){
	    //the 'from' vertex has been merged, we will create a new edge connecting the current_vertex to its 'to', unless the from is already the current_vertex
	    std::cerr << "... Removing";
	    vertices_for_new_edges.insert(tov);
	    _sgraph->graph()->removeEdge(ev);
	  }else if (vertices_transforms_merged.find(tov)!=vertices_transforms_merged.end() && 
		    tov!=current_vertex){
	    //the 'to' vertex has been merged, we will create a new edge connecting the current_vertex to its 'from', unless the to is already the current_vertex
	    std::cerr << "... Removing";
	    vertices_for_new_edges.insert(fromv);
	    _sgraph->graph()->removeEdge(ev);
	  }
	  std::cerr << std::endl;
	}
      }


      for (OptimizableGraph::VertexSet::iterator itv = vertices_for_new_edges.begin(); itv!= vertices_for_new_edges.end(); itv++){
	std::cerr << "Vertices for new edges: " << (*itv)->id()  << std::endl;
      }

      //adding edges with neighbors
      connectNeighbors(current_vertex, vertices_for_new_edges);

      //removing vertices from vsetToMerge (except the one remaining: current_vertex)
      OptimizableGraph::VertexSet::iterator itrm = vertices.find(current_vertex);
      vertices.erase(itrm);
      _vertices_to_merge.removeVertices(vertices);
      //removing merged vertices from graph except the reference vertex
      std::cerr << "Removing merged vertices from graph." << std::endl;
      for (std::map<OptimizableGraph::Vertex*, Eigen::Isometry2f>::iterator itv = vertices_transforms_merged.begin(); itv!= vertices_transforms_merged.end(); itv++){
	VertexSE2* vertex_to_remove = (VertexSE2*)itv->first;
	if (vertex_to_remove != current_vertex){
	  std::cerr << "Removing from graph vertex: " << vertex_to_remove->id()  << std::endl;
	  //in case we remove a fixed vertex the current is the new fixed one
	  current_vertex->setFixed(vertex_to_remove->fixed()); 
	  //finally we remove vertex from graph
	  _sgraph->removeVertex(vertex_to_remove);
	}else
	  _sgraph->addScanFromCloud(current_vertex);
      }
    }else{
      //removing vertices from vsetToMerge
      _vertices_to_merge.removeVertices(vertices);
    }
    std::cerr << "Finished merge" << std::endl;
  }
  */

  void GraphMerger::merge(OrderedVertexSet& vertices){
    if (vertices.size() < 2){
      std::cerr << "Need at least 2 vertices to merge" << std::endl;
      return;
    }
    //Vertex with lowest id is the reference in which we will merge the newest clouds (new overwrites old).
    //Vertex with the highest id will keep the merged cloud after the process.
    std::cerr << "Merging vertices: ";
    for (OrderedVertexSet::iterator itv = vertices.begin(); itv!= vertices.end(); itv++){
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *itv;
      std::cerr << v->id() << " ";
    }
    std::cerr << std::endl;

    //First vertex is the oldest
    OrderedVertexSet::iterator itv = vertices.begin();
    VertexSE2* v1 = (VertexSE2*) *itv;
    itv++;
    while (itv != vertices.end()){
      VertexSE2* v2 = (VertexSE2*) *itv;
      merge(v1, v2);

      /*
      //Logging merge
      Cloud2DWithTrajectory* cnew = (Cloud2DWithTrajectory*) _sgraph->vertexCloud(v2);
      char filename[100];
      sprintf(filename, "cloud_merge_after_%d_%d_%d.dat", _sgraph->currentVertex()->id(), v1->id(), v2->id());
      ofstream os(filename);
      cnew->save(os);
      */

      itv++;
      v1 = v2;
    }
    //At this point the newest vertex contains the merged cloud

    std::cerr << "Finished merge" << std::endl;
  }


  void GraphMerger::addVertexToMerge(VertexSE2* currentv){
    OptimizableGraph::VertexSet vertices_in_distance;
    VerticesFinder vf(_sgraph->graph());
    vf.findVerticesEuclideanDistance(vertices_in_distance, currentv, _vertex_merging_distance);

    addVerticesToMerge(vertices_in_distance);
  }

  void GraphMerger::addVerticesToMerge(OptimizableGraph::VertexSet& vertices){
    _vertices_to_merge.addVertices(vertices);
  }

  bool GraphMerger::triggerMerge(){
    OptimizableGraph::EdgeSet closures_in_buffer = _sgraph->closureBuffer().edgeSet();
    _vertices_to_merge.setClosuresBuffer(closures_in_buffer);

    _vertices_to_merge.init();

    bool merge_triggered = false;
    OptimizableGraph::VertexSet vertices;
    while (_vertices_to_merge.getNextVertices(vertices)) {
      merge_triggered = true;
      _sgraph->optimize(5);

      //Ordering vertices from oldest (lower id) to newest (higher id)
      OrderedVertexSet vertices_to_merge;
      for (OptimizableGraph::VertexSet::iterator itv = vertices.begin(); itv!= vertices.end(); itv++){
	OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *itv;
	vertices_to_merge.insert(v);
      }
      
      merge(vertices_to_merge);

      //Removing newest vertex from vertices_to_merge
      OrderedVertexSet::reverse_iterator itc = vertices_to_merge.rbegin();
      OptimizableGraph::Vertex* newest_vertex = (OptimizableGraph::Vertex*) *itc;

      OptimizableGraph::VertexSet vertices_to_remove, vertices_to_connect;
      for (OrderedVertexSet::iterator itv = vertices_to_merge.begin(); itv!= vertices_to_merge.end(); itv++){
	OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) *itv;
	if (v->id() != newest_vertex->id())
	  vertices_to_remove.insert(v);
      }
    
      computeNewNeighbors(vertices_to_remove, vertices_to_connect);
      connectNeighbors(newest_vertex, vertices_to_connect);
      _vertices_to_merge.removeVertices(vertices_to_remove);
      removeMergedVertices(vertices_to_remove);
      _sgraph->optimize(5);
    }

    _vertices_to_merge.clean();
    
    std::cerr << "TRIGGERING SET finished" << std::endl;
    return merge_triggered;
  }


  void GraphMerger::connectNeighbors(OptimizableGraph::Vertex* reference_vertex, OptimizableGraph::VertexSet& neighbors){

    CondensedGraphCreator cg_creator(_sgraph->graph());
    cg_creator.setGauge(reference_vertex);
    cg_creator.setVertices(neighbors);
    cg_creator.compute();

    OptimizableGraph::EdgeSet edges = cg_creator.getCondensedGraph();

    for (OptimizableGraph::EdgeSet::iterator ite=edges.begin(); ite!=edges.end(); ++ite){
      OptimizableGraph::Edge* ev = (OptimizableGraph::Edge*)*ite;
      _sgraph->graph()->addEdge(ev);
    }

  }
  /*
  void GraphMerger::connectNeighbors(OptimizableGraph::Vertex* reference_vertex, OptimizableGraph::VertexSet& neighbors){
    //connects reference_vertex with all the vertices from neighbors
    VertexSE2* refvse2 = (VertexSE2*) reference_vertex;
    for (OptimizableGraph::VertexSet::iterator itv = neighbors.begin(); itv!= neighbors.end(); itv++){
      VertexSE2* curvse2 = (VertexSE2*)*itv;
	
      //creating edge from refvse2 to curvse2 unless this edge already exists
      bool exists = false;
      OptimizableGraph::EdgeSet tmp(refvse2->edges());
      for (OptimizableGraph::EdgeSet::iterator ite=tmp.begin(); ite!=tmp.end(); ++ite){
	OptimizableGraph::Edge* ev = (OptimizableGraph::Edge*)*ite;
	VertexSE2* fromv = (VertexSE2*) ev->vertices()[0];
	VertexSE2* tov = (VertexSE2*) ev->vertices()[1];		  
	    
	if ((fromv == refvse2 && tov == *itv) ||
	    (fromv == *itv && tov == refvse2)){
	  exists = true;

	  std::cerr << "Edge from: " << fromv->id() << " to: " << tov->id() << " already exists" << std::endl;
	
	  SE2 initial_relative_transf = fromv->estimate().inverse() * tov->estimate();
	
	  _c2DAligner->setReference(_sgraph->vertexCloud(fromv));
	  _c2DAligner->setCurrent(_sgraph->vertexCloud(tov));
	  _c2DAligner->compute(initial_relative_transf.toIsometry().cast<float>());
	  bool success = _c2DAligner->validate();
	  //success = false; //always add edges from relative pose estimation (avoids ruining loop closures?)
	  if (success){
	    std::cerr << "Recomputing." << std::endl;
	    SE2 relative_transf(_c2DAligner->T().cast<double>());
	    Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	    EdgeSE2 *evse2 = (EdgeSE2*) ev;
	    evse2->setMeasurement(relative_transf);
	    evse2->setInformation(inf);
	  
	  }

	  break;
	}
      }
      //TODO:if exists -> not add but change info?
      if (exists || refvse2->id() == curvse2->id()){
	continue;		
      }

      std::cerr << "Computing new edge from: " << refvse2->id() << " to: " << curvse2->id() << std::endl;
      SE2 initial_relative_transf = refvse2->estimate().inverse() * curvse2->estimate();
    
      _c2DAligner->setReference(_sgraph->vertexCloud(refvse2));
      _c2DAligner->setCurrent(_sgraph->vertexCloud(curvse2));
      _c2DAligner->compute(initial_relative_transf.toIsometry().cast<float>());
      bool success = _c2DAligner->validate();
      //success = false; //always add edges from relative pose estimation (avoids ruining loop closures?)
      if (success){
	std::cerr << "adding edge with neighbor from alignment." << std::endl;
	SE2 relative_transf(_c2DAligner->T().cast<double>());
	//Eigen::Matrix3d inf = 100*Eigen::Matrix3d::Identity();
	Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	_sgraph->addEdge(refvse2, curvse2, relative_transf, inf);
      }else{
	std::cerr << "adding edge with neighbor from pose estimation." << std::endl;
	Eigen::Matrix3d inf = 1000*Eigen::Matrix3d::Identity();
	_sgraph->addEdge(refvse2, curvse2, initial_relative_transf, inf);
      }
    }

    // //connecting also neighbors with each other
    // for (std::set<OptimizableGraph::Vertex*>::iterator it1 = neighbors.begin(); it1!= neighbors.end(); it1++){
    //   VertexSE2* v1 = (VertexSE2*)*it1;
    //   for (std::set<OptimizableGraph::Vertex*>::iterator it2 = std::next(it1, 1); it2!= neighbors.end(); it2++){
    //     VertexSE2* v2 = (VertexSE2*)*it2;
    //     //creating edge from v1 to v2 unless this edge already exists
    //     bool exists = false;
    //     OptimizableGraph::EdgeSet tmp(v1->edges());
    //     for (OptimizableGraph::EdgeSet::iterator ite=tmp.begin(); ite!=tmp.end(); ++ite){
    // 	OptimizableGraph::Edge* ev = (OptimizableGraph::Edge*)*ite;
    // 	OptimizableGraph::Vertex* fromv = (OptimizableGraph::Vertex*) ev->vertices()[0];
    // 	OptimizableGraph::Vertex* tov = (OptimizableGraph::Vertex*) ev->vertices()[1];		  
	
    // 	if ((fromv == v1 && tov == v2) ||
    // 	    (fromv == v2 && tov == v1)){
    // 	  exists = true;
    // 	  break;
    // 	}
    //     }
    //     if (exists)
    // 	continue;

    //     std::cerr << "adding edge between neighbor " << v1->id() << "and neighbor " << v2->id() << " from pose estimation." << std::endl;
    //     SE2 relative_transf = v1->estimate().inverse() * v2->estimate();
    //     Eigen::Matrix3d inf = 1000*Eigen::Matrix3d::Identity();
    //     _sgraph->addEdge(v1, v2, relative_transf, inf);
    //   }

    // }

    }*/

}
